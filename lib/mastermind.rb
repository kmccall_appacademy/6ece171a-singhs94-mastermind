class Code
  attr_reader :pegs

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }
  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    pegs = string.split('').map do |letter|
      raise 'error' unless PEGS.has_key?(letter.upcase)
      PEGS[letter.upcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times do
      pegs.push(PEGS[PEGS.values.sample])
    end
    Code.new(pegs)
  end

  def [](index)
    pegs[index]
  end

  def exact_matches(other_code)
    count = 0
    i = 0
    pegs.each do |color|
      if color == other_code[i]
        count += 1
      end
      i += 1
    end
    count
  end
  #COME BACK TO THIS PART
  def near_matches(other_code)
    # count = 0
    # self.each_with_index do |color, index|
    #   if other_code.include?(color) && other_code[index] != color
    #     count += 1
    #   end
    # end
    # count
    other_color_counts = other_code.color_counts

    near_matches = 0
    self.color_counts.each do |color, count|
      next unless other_color_counts.has_key?(color)

     # Give credit for near matches up to `count`
      near_matches += [count, other_color_counts[color]].min
    end

    near_matches - self.exact_matches(other_code)
    end

  def ==(code)
    return true if exact_matches(code) == pegs.length
    false
  end

  protected

  def color_counts
    color_counts = Hash.new(0)

    @pegs.each do |color|
      color_counts[color] += 1
    end

    color_counts
  end

end


class Game
  attr_reader :secret_code

def initialize(secret_code = Code.random)
  @secret_code = secret_code
end

def get_guess
  puts "Make a guess"
  input = gets.chomp
  Code.parse(input)
end

def display_matches(guess)
  exact_matches = @secret_code.exact_matches(guess)
  near_matches = @secret_code.near_matches(guess)

  puts "You got #{exact_matches} exact matches!"
  puts "You got #{near_matches} near matches!"
end

end
